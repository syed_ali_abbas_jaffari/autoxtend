
## Introduction to AUTO-XTEND

AUTO-XTEND toolchain provides an embedded software development environment based on the AUTOSAR standard 4.2 (classic, i.e. not adaptive). The toolchain provides support at different stages in the ECU development process, e.g., application development and embedded platform development. The toolchain is designed with special focus on AUTOSAR design methodology, i.e. the application timing and scheduling analysis is not the target of the toolchain. The main advantage of the toolchain, compared to the existing AUTOSAR (commercial and free) tools, is that it enables extension of the AUTOSAR meta-model including platform development and 3rd party toolchain integration.

The toolchain consists of 3 tools which can be used to create AUTOSAR based applications for embedded platforms like ARM, PPC and Renesas including Raspberry Pi 1, 2 and Zero (i.e. for BCM2835 SOC):

1. **AUTOSAR App Studio** for application design. Source @ [https://bitbucket.org/syed_ali_abbas_jaffari/autoxtend_app_studio](https://bitbucket.org/syed_ali_abbas_jaffari/autoxtend_app_studio)
2. **AUTOSAR Design Studio** for code generation. Source @ [https://bitbucket.org/syed_ali_abbas_jaffari/autoxtend_design_studio](https://bitbucket.org/syed_ali_abbas_jaffari/autoxtend_design_studio)
3. **AUTOSAR Core** ([ArcCore 12.0](https://www.arccore.com/)) for embedded OS support. Source @ [https://bitbucket.org/syed_ali_abbas_jaffari/autoxtend_arccore](https://bitbucket.org/syed_ali_abbas_jaffari/autoxtend_arccore)

The tools in AUTO-XTEND are written in Qt/C++ to provide cross-platform development of the toolchain. This enables faster toolchain run-times compared to existing tools. However, due to the same reason the tools are hard-coded to work with AUTOSAR 4.2 meta-model, i.e., the meta-model elements are part of the code. This was necessary since Qt does not yet support model-based development like Eclipse Modeling Framework. In other words, the APIs in AUTO-XTEND tools use primitive DOM based APIs to explore the AUTOSAR models and meta-model. Moroever, the tools in AUTO-XTEND are written to achieve code generation as early as possible. Therefore, please do not expect very clear and structured code with proper comments (I am not very proud of this code of mine). Help and suggestions are most welcome. To get further details about the toolchain, please refer to the [PhD dissertation](https://nbn-resolving.org/urn:nbn:de:hbz:386-kluedo-53040). (Note: To avoid contamination from changes which are not standard compliant, the 2nd case study mentioned in the thesis is excluded from this repository.)

## Wish to contribute?

You are welcome to contribute in improving and extending the software. To do that, you can request for a branch or report bugs (with procedure to reproduce the bug) to the author, Ali Syed @ [syed.ali.abbas.jaffari@gmail.com](mailto:syed.ali.abbas.jaffari@gmail.com). 

## Copyright and License

Copyright (C) 2016, Ali Syed, Germany
Contact: [syed.ali.abbas.jaffari@gmail.com](mailto:syed.ali.abbas.jaffari@gmail.com)

AUTO-XTEND is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. AUTO-XTEND is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the [GNU General Public License](https://www.gnu.org/licenses/gpl.txt) for more details.

**Disclaimer:** The rights for the operating system core from ArcCore and AUTOSAR meta-model provided as part of this toolchain are reserved by ArcCore AB, Sweden. The licensing statement from ArcCore is provided as under:

Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
Contact: [contact@arccore.com](mailto:contact@arccore.com)
You may ONLY use this file:

1. if you have a valid commercial ArcCore license and then in accordance with the terms contained in the written license agreement between you and ArcCore, or alternatively
2. if you follow the terms found in GNU General Public License version 2 as published by the Free Software Foundation and appearing in the file LICENSE.GPL included in the packaging of this file or [here](http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt).

